import React, { useEffect, useState } from "react";
import { BinarySearchTree, useBinarySearchTree } from "react-tree-vis";

export interface BTSProps {}
// CHECK ARRAY TO SIMILAR NUMBERS
const checkNumber = (arr: number[], num: number) => {
  return arr?.filter((item: number) => item === num);
};
// GET NUMBERS OF ARRAY THEN CHEK BY checkNumbers AND GENERATE NEW NUMBERS
const getNum = (arr: number[]): number => {
  let randomizer = Math.round(Math.random() * 100);
  let dessisioner = Math.round(Math.random() * 100);
  if (dessisioner % 2 === 0) {
    randomizer -= 100;
  }
  if (!checkNumber(arr, randomizer).length) {
    return randomizer;
  } else {
    return getNum(arr);
  }
};

const genInitialNums = () => {
  const arr: number[] = [];
  const arrPusher = (): number[] => {
    arr.push(getNum(arr));
    if (arr.length === 10) {
      return arr;
    } else {
      return arrPusher();
    }
  };
  return arrPusher();
};

export default function BTS() {
  const [numbers, setNumbers] = useState<number[]>([]);

  const onKeyDown = (event: KeyboardEvent | React.KeyboardEvent) => {
    if (event.code === "Space") {
      setNumbers([...numbers, getNum(numbers)]);
    }
  };

  useEffect(() => {
    // initiate the event handler
    window.addEventListener("keydown", onKeyDown);
    // this will clean up the event every time the component is re-rendered
    return function cleanup() {
      window.removeEventListener("keydown", onKeyDown);
    };
  });
  useEffect(() => {
    setNumbers(genInitialNums());
  }, []);

  return (
    <div onKeyDown={onKeyDown}>
      <BinarySearchTree data={numbers} />
    </div>
  );
}
